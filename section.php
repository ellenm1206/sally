<?php
/*
Section: Books Layout
Author: PageLines
Author URI: http://www.pagelines.com
Version: 1.2.0

Description: PageLines Starter Section (Includes "Pull Quote" Example)
Class Name: BooksLayout

*/

/*
 * PageLines Headers API
 * 
 *  Sections support standard WP file headers (http://codex.wordpress.org/File_Header) with these additions:
 *  -----------------------------------
 * 	 - Section: The name of your section.
 *	 - Class Name: Name of the section class goes here, has to match the class extending PageLinesSection.
 *	 - Cloning: (bool) Enable cloning features.
 *	 - Depends: If your section needs another section loaded first set its classname here.
 *	 - Workswith: Comma seperated list of template areas the section is allowed in.
 *	 - Failswith: Comma seperated list of template areas the section is NOT allowed in.
 *	 - Demo: Use this to point to a demo for this product.
 *	 - External: Use this to point to an external overview of the product
 *	 - Long: Add a full description, used on the actual store page on http://www.pagelines.com/store/
 *
 */

/**
 *
 * Section Class Setup
 * 
 * Name your section class whatever you want, just make sure it matches the 
 * "Class Name" in the section meta (above)
 * 
 * File Naming Conventions
 * -------------------------------------
 *  section.php 		- The primary php loader for the section.
 *  style.css 			- Basic CSS styles contains all structural information, no color (autoloaded)
 *  images/				- Image folder. 
 *  thumb.png			- Primary branding graphic (300px by 225px - autoloaded)
 *  screenshot.png		- Primary Screenshot (300px by 225px)
 *  screenshot-1.png 	- Additional screenshots: (screenshot-1.png -2 -3 etc...optional).
 *  icon.png			- Portable icon format (16px by 16px)
 *	color.less			- Computed color control file (autoloaded)
 *
 */
class BooksLayout extends PageLinesSection {

	const version = '1.0';

function section_styles(){
		
		wp_enqueue_script( 'isotope', $this->base_url.'/js/jquery.isotope.min.js', array( 'jquery' ), self::version, true);
		wp_enqueue_script( 'easing', $this->base_url.'/js/jquery.easing.1.3.min.js',array( 'jquery' ), self::version, true);
		wp_enqueue_script( 'mosaic', $this->base_url.'/js/mosaic.1.0.1.min.js',array( 'jquery' ), self::version, true);
		wp_enqueue_style('mosaic', $this->base_url.'/css/mosaic.css');
		
		}

	/**
	 *
	 * Section Variable Glossary (Auto Generated)
	 * ------------------------------------------------
	 *  $this->id			- The unique section slug & folder name
	 *  $this->base_url 	- The root section URL
	 *  $this->base_dir 	- The root section directory path
	 *  $this->name 		- The section UI name
	 *  $this->description	- The section description
	 *  $this->images		- The root section images URL
	 *  $this->icon 		- The section icon url
	 *  $this->screen		- The section screenshot url 
	 *  $this->oset			- Option settings array... needed for 'ploption' (contains clone_id, post_id)
	 * 
	 * 	Advanced Variables
	 * 		$this->view				- If the section is viewed on a page, archive, or single post
	 * 		$this->template_type	- The PageLines template type
	 */

	/**
	 *
	 * Section API - Methods & Functions
	 * 
	 * Below we'll give you a listing of all the available 
	 * Section 'methods' or functions, and other techniques.
	 * 
	 * Please reference other PageLines sections for ideas/tips on how
	 * to use more advanced functionality.
	 *
	 */

		/**
		 *
		 * Persistent Section Code 
		 * 
		 * Use the 'section_persistent()' function to add code that will run on every page in your site & admin
		 * Code here will run ALL the time, and is useful for adding post types, options etc.. 
		 *
		 */
		function section_persistent(){
		
		} 

		/**
		 *
		 * Site Head Section Code 
		 * 
		 * Code added in the 'section_head()' function will be run during the <head> element of your site's
		 * 'front-end' pages. You can use this to add custom Javascript, or manually add scripts & meta information
		 * It will *only* be loaded if the section is present on the page template.
		 *
		 */
		function section_head(){
			?>
			<script>
			jQuery(document).ready(function(){
				// Isotope Center Container

			    jQuery.Isotope.prototype._getCenteredMasonryColumns = function() {
			    this.width = this.element.width();

			    var parentWidth = this.element.parent().width();

			                  // i.e. options.masonry && options.masonry.columnWidth
			    var colW = this.options.masonry && this.options.masonry.columnWidth ||
			                  // or use the size of the first item
			                  this.$filteredAtoms.outerWidth(true) ||
			                  // if there's no items, use size of container
			                  parentWidth;

			    var cols = Math.floor( parentWidth / colW );
			    cols = Math.max( cols, 1 );

			    // i.e. this.masonry.cols = ....
			    this.masonry.cols = cols;
			    // i.e. this.masonry.columnWidth = ...
			    this.masonry.columnWidth = colW;
			  };

			  jQuery.Isotope.prototype._masonryReset = function() {
			    // layout-specific props
			    this.masonry = {};
			    // FIXME shouldn't have to call this again
			    this._getCenteredMasonryColumns();
			    var i = this.masonry.cols;
			    this.masonry.colYs = [];
			    while (i--) {
			      this.masonry.colYs.push( 0 );
			    }
			  };

			  jQuery.Isotope.prototype._masonryResizeChanged = function() {
			    var prevColCount = this.masonry.cols;
			    // get updated colCount
			    this._getCenteredMasonryColumns();
			    return ( this.masonry.cols !== prevColCount );
			  };

			  jQuery.Isotope.prototype._masonryGetContainerSize = function() {
			    var unusedCols = 0,
			        i = this.masonry.cols;
			    // count unused columns
			    while ( --i ) {
			      if ( this.masonry.colYs[i] !== 0 ) {
			        break;
			      }
			      unusedCols++;
			    }

			    return {
			          height : Math.max.apply( Math, this.masonry.colYs ),
			          // fit container to columns that have been used;
			          width : (this.masonry.cols - unusedCols) * this.masonry.columnWidth
			        };
			  };

			 

			  // Call Isotope
			 
			  	var mycontainer = jQuery('.books');

			      // add randomish size classes
			      mycontainer.find('.item').each(function(){
			        var $this = jQuery(this),
			            number = parseInt( $this.find('.item-title').text(), 10 );
			        if ( number % 7 % 2 === 1 ) {
			          $this.addClass('width2');
			        }
			        if ( number % 3 === 0 ) {
			          $this.addClass('height2');
			        }
			      });
			 
		       mycontainer.isotope({
	     			itemSelector: '.item',
	     			 masonry: {
	    			columnWidth: 25,
	    			

	  				},
	     		
		  		}).imagesLoaded( function() {
		  			
		    	// trigger again after images have loaded
		    	mycontainer.isotope('reLayout');
		    		
		  		});
		

			   // filter items when filter link is clicked
			jQuery('.books-nav-wrap .options a').click(function(){
			  var selector = jQuery(this).attr('data-filter');
			  mycontainer.isotope({ filter: selector });
			    
			  var optionSet=jQuery(this).parents(".books-nav-wrap .options");
				if(jQuery(this).hasClass("selected")){
					return false}
					jQuery(optionSet).find(".selected").removeClass("selected");
					jQuery(this).addClass("selected");
				return false;
			  });

			// End Isotope


			// Mosaic Hover
	     	jQuery('.cover').mosaic({
				animation	:	'slide',	//fade or slide
				speed		: 300,
    			anchor_x	: 'right',
    			anchor_y	: 'top',
				hover_x		:	'358px'		//Horizontal position on hover
			});
	     	jQuery('.cover2').mosaic({
				animation	:	'slide',	//fade or slide
				speed		: 300,
				opacity		: 1,
					preload		: 0,
				anchor_x	: 'right',
				anchor_y	: 'top',
				hover_x		:	'250px'		//Horizontal position on hover
			});
	 
	 }); 
	</script>
	<?php
			
		} 

		/**
		 *
		 * Section Template
		 * 
		 * The 'section_template()' function is the most important section function. 
		 * Use this function to output all the HTML for the section on pages/locations where it's placed.
		 * 
		 * Here we've included some example processing and output for a 'Pull Quote' section.
		 *
		 */
	 	function section_template(  ) { 
	 		
		 		$this->books_nav(); 
		 		echo '<div class="books-wrap"><div class="books cs-style-4  clearfix">';		
		 		$this->all_books_loop();
		 		echo '</div></div>';
		 			

			
			 }
 
		
		function books_nav() {

			?> <nav class="books-nav-wrap">
           <ul class="options clearfix">
			
			<?php 
			
				echo'<li><a href="#show-all" data-filter="*" class="selected">Show All</a></li>';
				
				$cat_idObj = get_category_by_slug('book-pages'); 
	  			$cat_id = $cat_idObj->term_id;
				$cat_args=array(
	  				'child_of'  => $cat_id,
	  			);
			//	$v = get_query_var( 'cat' );	
				$categories=  get_categories($cat_args); 
	  					foreach ($categories as $category) {
					 ?>

			    	<li><a href="#" data-filter=".<?php echo $category->slug?>"><?php echo $category->name?></a></li>

			    <?php } ?>

				</ul>
	           
	        </nav>
	        <?php
	}

	function all_books_loop() {
		global $post;
			 
		$args2 = array(
				'parent' => -1,
				'exclude_tree' => '',
				
				'post_type' => 'page',
				'post_status' => 'publish',
				'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => 'book-pages',
					'include_children' => true,
				)
			)			

		); 
			 
		$books = new WP_Query($args2);
		if ( $books->have_posts() ) : while ( $books->have_posts() ) : $books->the_post(); 
			$terms = get_the_category($post->ID );
			$terms_string = '';
	 		foreach ( $terms as $term ) :     
      			$terms_string = $terms_string.$term->slug.' '; 
         	endforeach;
        if(get_field('horizontal')) { 	 
			$image = get_field('horizontal' , $post->ID);
			$style = 'style="width: 358px; height: 315px;"';
		} elseif(get_field('vertical')) {
			$image= get_field('vertical' , $post->ID);
			$style = 'style="width: 236px; height: 315px;"';
		} else {
			$image= null;
			$style = null;
		}
	
		
		printf('<div class="item %s" %s>', $terms_string, $style);
		
		if(get_field('horizontal')) {
		echo '<div class="book-image mosaic-block cover">';

	 ?>
		
			<div class="mosaic-overlay"><img src="<?php echo $image['url']; ?>" /></div>
			<a href="<?php echo get_permalink(); ?>" class="mosaic-backdrop papers">
				<div class="details">
					<h3><?php the_title(); ?></h3>
					<p><?php echo $post->post_excerpt ?></p>
					<div class="bottom-open">Open Here</div>
					
				</div>
			</a>
			
      <?php 
      } else {
      	echo '<div class="book-image mosaic-block cover2">';

	 ?>
		
			<div class="mosaic-overlay"><img src="<?php echo $image['url']; ?>" /></div>
			<a href="<?php echo get_permalink(); ?>" class="mosaic-backdrop papers2">
				<div class="details">
					<h3><?php the_title(); ?></h3>
					<p><?php echo $post->post_excerpt ?></p>
					<div class="bottom-open">Open Here</div>
					
				</div>
			</a>
			
      <?php 

      }     
	echo'</div></div>';
	endwhile; ?>
	<?php else: ?>
	<?php endif;	
	}
	
	
	} /* End of section class - No closing php tag needed */